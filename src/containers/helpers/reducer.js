import {
    GET_BLOG_LIST_SUCCESS
} from './constants';

const initialState = {
    items: [
        { name : 'a' }, { name : 'a' }, { name : 'a' },
        { name : 'a' }, { name : 'a' }, { name : 'a' },
        { name : 'a' }, { name : 'a' }, { name : 'a' },
        { name : 'a' }, { name : 'a' }, { name : 'a' },
    ],
    blogList: [],
};

export const helpersReducer = ( state = initialState, action ) => {
    switch( action.type ) {
        case GET_BLOG_LIST_SUCCESS:
            return {
                ...state,
                blogList: action.payload
            };
        default:
            return {
                ...state,
            };
    }
};

export default helpersReducer;