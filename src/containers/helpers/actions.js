import axiosApi from '../../axiosApi';
import {
    GET_BLOG_LIST_SUCCESS
} from './constants';

const getBlogListSuccess = payload => ({
    type: GET_BLOG_LIST_SUCCESS,
    payload
});

export const getBlogList = () => dispatch => {

    return axiosApi({
        method: 'GET',
        url: '/blogs'
    })
    .then(resp => {
        console.log({resp})
        dispatch(getBlogListSuccess(resp.data.blogs));
    })
    .catch(err => console.log({err}))
}