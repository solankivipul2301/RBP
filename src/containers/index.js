import Home from './Home/index';
import Dashboard from './Dashboard';

import NotFoundPage from './NotFoundPage';

export {
    Home, Dashboard, NotFoundPage,
};
