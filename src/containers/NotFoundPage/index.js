import React, { Component } from 'react';

export class NotFoundPage extends Component {
    render () {
        return (
            <React.Fragment>
                Page not found
            </React.Fragment>
        );
    }
}

export default NotFoundPage;