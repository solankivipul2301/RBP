import axiosApi from "../../axiosApi";

export function doGetContactsByUserIdInvite(id,inviteId) {
    return axiosApi({
      method: 'GET',
      url: `users/${id}/invite`,
      params: {
        filter: {
          include: ['meetings'],
          where: {
            id: inviteId,
          },
        },
      },
    })
}
export function createUserFromContact(id) {
    return axiosApi({
      method: 'POST',
      url: `Invitees/createUserFromContact`,
      data: { id: id },
    })
}
