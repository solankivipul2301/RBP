/* eslint-disable react/prop-types,import/first,no-nested-ternary,no-console,react/sort-comp,max-len,no-confusing-arrow,no-underscore-dangle,react/no-multi-comp,no-undef,class-methods-use-this,no-return-assign,react/jsx-no-bind,no-param-reassign,radix,no-lonely-if,react/no-did-mount-set-state,no-mixed-operators,no-labels,no-restricted-syntax,react/forbid-prop-types,react/require-default-props,no-plusplus,no-shadow,react/no-children-prop,camelcase,jsx-a11y/label-has-for,array-callback-return,css-modules/no-undef-class,no-useless-constructor,no-unused-vars,consistent-return */
/* eslint-disable jsx-a11y/no-static-element-interactions, no-useless-escape, no-sequences */
/* eslint-disable react/no-unescaped-entities, react/jsx-no-target-blank, react/no-array-index-key, no-unused-expressions */
import axios from 'axios';
import { sessionService, loadSession } from 'redux-react-session';
const API_URL = "https://jsonplaceholder.typicode.com/posts"

export function storeUserData(data) {
  return {
    type: 'STORE_PRODUCT_DATA',
    data,
  };
}
export function getUser() {
  return dispatch => axios({
    method: 'get',
    url: `${API_URL}`,

  }).then((resp) => {
    if (resp && resp.data) {
      dispatch(storeUserData(resp.data));
      return resp.data;
    }
    return resp;
  }).catch((error, code, status) => error && error.response && error.response.data);
}
