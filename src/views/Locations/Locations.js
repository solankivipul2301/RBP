import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import {Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Alert} from 'reactstrap';
import {connect} from 'react-redux';
import {doGetLocationList, doAddLocationList, doDeleteLocationById, doUpdateLocationById} from './action';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {AvForm, AvGroup, AvFeedback, AvInput, AvField} from 'availity-reactstrap-validation';
import SweetAlert from 'react-bootstrap-sweetalert';
import _ from 'lodash';

class Locations extends Component {
	constructor(props) {
		super(props);
		this.state = {
			locationList: [],
			modal: false,
			editRecord: {},
			isError: false,
			showSuccess: false,
			showError: false,
			showConfirmation: false,
			isAddReqLoading: false,
			isAddSuccess: false,
			isEdit: false,
			isAddError: false,
			isLoading:true,
		};
	}

	componentDidMount() {
		this.getLocationList();
	}

	getLocationList = () => {
		this.setState({
			error: false
		});
		this.props.doGetLocationList().then(resp => {
			this.setState({
				error: false,
				locationList: resp.data
			});
			setTimeout(() => {
				this.setState({
					isLoading: false
				});
			}, 5);
		}).catch(error => {
			this.setState({
				error: true
			});
		})
	};
	toggleModal = () => {
		this.setState({
			modal: !this.state.modal
		});
	};
	handleValidSubmit = (event, values) => {
		let self = this;
		let {locationList, isEdit, editRecord} = this.state;
		this.setState({
			isAddReqLoading: true,
			isAddSuccess: false,
			isAddError: false
			});
			let promises = isEdit && editRecord && editRecord.id ? this.props.doUpdateLocationById(editRecord.id, values) : this.props.doAddLocationList(values);
		promises.then(resp => {
			if (isEdit && editRecord && editRecord.id) {
				let index = _.findIndex(locationList, {"id": editRecord.id});
				locationList[index] = resp.data
			}
			else {
				locationList.push(resp.data);
			}
			this.setState({
				isAddReqLoading: false,
				isAddError: false,
				modal: false,
				isAddSuccess: true,
				editRecord: {},
				locationList: locationList
			});
			setTimeout(() => {
				self.setState({
					isAddSuccess: false,
					isEdit: false,
				})
			}, 5000)
		}).catch(err => {
			this.setState({
				isAddReqLoading: false,
				isAddError: true
			});
		})
	};
	handleInvalidSubmit = (event, errors, values) => {
		this.setState({
			isAddReqLoading: false,
			isAddError: true
		});
	};

	actionFormat = (cell, row) => {
		return <div>
			<Button color="primary" type="button" className="fa fa-pencil"
							onClick={ () => this.askEdit(row)}></Button>&nbsp;&nbsp;&nbsp;
			<Button color="danger" type="button" className="fa fa-trash" onClick={ () => this.askDelete(row)}></Button>

		</div>
	};

	addressFormatter = (cell, row) => {
		return row.address1 + "</br> " +
			row.address2 +
			(row.address2 && row.address2.length ? "</br>" : "") +
			row.zip +
			(row.zip && row.zip.length ? "</br>" : "") +
			row.state +
			(row.state && row.state.length ? "</br>" : "") +
			row.country +
			(row.country && row.country.length ? "</br>" : "");
	};
	askDelete = (row) => {
		this.setState({
			showConfirmation: true,
			deleteRecord: row
		});
	};
	askEdit = (row) => {
		this.setState({
			editRecord: row,
			isEdit: true,
			modal: true
		});
	};

	onConfirmation = () => {
		let {deleteRecord, locationList} = this.state;
		this.props.doDeleteLocationById(deleteRecord.id).then(resp => {
			_.remove(locationList, {"id": deleteRecord.id});
			this.setState({
				showSuccess: true,
				showConfirmation: false,
				locationList: locationList
			})
		}).catch(error => {
			this.setState({
				showError: true
			})
		})
	};

	onCancel = () => {
		this.setState({
			showConfirmation: false
		});
	};

	render() {
		let {locationList} = this.props;
		let options = {
			noDataText: 	this.state.isLoading? <i className="fa fa-spinner fa-spin p-1" />:'Location Not Found'
		};
		
		return (
			<div className="animated fadeIn">
				<div className="card">
					<div className="card-header">
						<i className="icon-location-pin" /> Locations List
						<div className="pull-right">
							<Button onClick={this.toggleModal} color="primary" type="button" className="btn-outline-primary"><i
								className="icon icon-plus mr-1" />Add Location</Button>
						</div>
					</div>
					<div className="card-body">
						
							{
								this.state.isAddSuccess && <Alert color="success" className="mb-3 col-md-12 text-center">
									<div>Location successfully {this.state.isEdit ? "updated" : "added"}</div>
								</Alert>
							}
							<BootstrapTable data={locationList} striped={false} hover={true} search pagination options={options}
															multiColumnSearch>
								<TableHeaderColumn dataField="name" isKey={true} dataAlign="center"
																	 dataSort={true}>Name</TableHeaderColumn>
								<TableHeaderColumn dataField="type" dataAlign="center" dataSort={true}>Type</TableHeaderColumn>
								<TableHeaderColumn dataField="country" dataAlign="center" dataSort={true}>Country</TableHeaderColumn>
								<TableHeaderColumn dataField="state" dataAlign="center" dataSort={true}
																	 dataFormat={ this.addressFormatter }>Address</TableHeaderColumn>
								<TableHeaderColumn dataField="phone" dataAlign="center" dataSort={true}>Phone Number</TableHeaderColumn>
								<TableHeaderColumn dataField="phone" dataAlign="center" dataSort={false}
																	 dataFormat={ this.actionFormat }></TableHeaderColumn>
							</BootstrapTable>
						
					</div>
				</div>
				<SweetAlert
					warning
					showCancel
					confirmBtnText="Yes, delete it!"
					cancelBtnText="Cancel"
					confirmBtnBsStyle="danger"
					cancelBtnBsStyle="default"
					show={this.state.showConfirmation}
					title="Are you Sure?"
					text="You want to delete this location."
					onConfirm={this.onConfirmation}
					onCancel={this.onCancel}
				/>
				<SweetAlert show={this.state.showSuccess} success title="Success" onConfirm={() => {
					this.setState({
						showSuccess: false
					})
				}}>
					Operation is successfully
				</SweetAlert>
				<SweetAlert show={this.state.showError} danger title="Error while processing" onConfirm={() => {
					this.setState({
						showError: false
					})
				}}>
					Error while processing your request.
				</SweetAlert>
				<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size="lg">
					<AvForm onValidSubmit={this.handleValidSubmit} onInvalidSubmit={this.handleInvalidSubmit}>
						<ModalHeader toggle={this.toggle}>{this.state.isEdit ? "Update" : "Add"} Locations</ModalHeader>
						<ModalBody>
							{
								this.state.isAddError && <Alert color="danger" className="mb-3 col-md-12 text-center">
									<div>Error while {this.state.isEdit ? "updating" : "adding"} location</div>
								</Alert>
							}
							<AvGroup className="mb-3">
								<Label for="name">Location Name</Label>
								<AvInput name="name" id="name" placeholder="Location Name" value={this.state.editRecord.name}
												 type="text" required/>
								<AvFeedback>Invalid Name</AvFeedback>
							</AvGroup>

							{/*<AvGroup className="mb-3">
							 <Label for="type">Location Type</Label>
							 <AvInput name="type" id="type" placeholder="Location Type" value={this.state.editRecord.type}
							 type="text" required/>
							 <AvFeedback>Invalid Location Type</AvFeedback>
							 </AvGroup>*/}
							<AvField type="select" name="type" label="Location Type" value={this.state.editRecord.type}>
								<option value="hub">Hub</option>
								<option value="store">Store</option>
								<option value="office">Office</option>
							</AvField>

							<AvGroup className="mb-3">
								<Label for="address1">Address Line 1</Label>
								<AvInput name="address1" id="address1" placeholder="Address Line 1"
												 value={this.state.editRecord.address1}
												 type="text" required/>
								<AvFeedback>Invalid Address Line 1</AvFeedback>
							</AvGroup>

							<AvGroup className="mb-3">
								<Label for="address2">Address Line 2</Label>
								<AvInput name="address2" id="address2" placeholder="Address Line 2"
												 value={this.state.editRecord.address2}
												 type="text"/>
								<AvFeedback>Invalid Address Line 2</AvFeedback>
							</AvGroup>

							<AvGroup className="mb-3">
								<Label for="city">City</Label>
								<AvInput name="city" id="city" placeholder="City" value={this.state.editRecord.city}
												 type="text"/>
								<AvFeedback>Invalid City</AvFeedback>
							</AvGroup>
							<AvGroup className="mb-3">
								<Label for="zip">Postal Code</Label>
								<AvInput name="zip" id="zip" placeholder="Postal Code" value={this.state.editRecord.zip}
												 type="text"/>
								<AvFeedback>Invalid Postal Code</AvFeedback>
							</AvGroup>

							<AvGroup className="mb-3">
								<Label for="state">State</Label>
								<AvInput name="state" id="state" placeholder="State" value={this.state.editRecord.state}
												 type="text" required/>
								<AvFeedback>Invalid State</AvFeedback>
							</AvGroup>

							<AvField type="select" name="country" label="Country" value={this.state.editRecord.country}>
								<option value="india">India</option>
								<option value="uk">UK</option>
							</AvField>

							{/*<AvGroup className="mb-3">
								<Label for="country">Country</Label>
								<AvInput name="country" id="country" placeholder="Country" value={this.state.editRecord.country}
												 type="text" required/>
								<AvFeedback>Invalid Country</AvFeedback>
							</AvGroup>*/}

							<AvGroup className="mb-3">
								<Label for="phone">Phone</Label>
								<AvInput name="phone" id="phone" placeholder="Phone" value={this.state.editRecord.phone}
												 type="text" required
												 validate={{pattern: {value: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im}}}/>
								<AvFeedback>Invalid Phone</AvFeedback>
							</AvGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="danger" type="button" onClick={this.toggleModal}>Cancel</Button>{' '}
							<Button color="primary" disabled={this.state.isAddReqLoading}>{this.state.isAddReqLoading &&
							<i className="fa fa-spinner fa-spin p-1" />}{this.state.isEdit ? "Update" : "Save"}</Button>
						</ModalFooter>
					</AvForm>
				</Modal>
			</div>
		);
	}
}

const mapDispatchToProps = {
	doGetLocationList: () => doGetLocationList(),
	doDeleteLocationById: (id) => doDeleteLocationById(id),
	doUpdateLocationById: (id, data) => doUpdateLocationById(id, data),
	doAddLocationList: (data) => doAddLocationList(data)
};
const mapStateToProps = state => ({
	locationList: state.location.location
});

export default connect(mapStateToProps, mapDispatchToProps)(Locations);
