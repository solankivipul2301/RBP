/* eslint-disable import/first */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { sessionService } from 'redux-react-session';
import rootReducer from '../reducers';
export default function configureStore(initialState) {
	const store = createStore(
		rootReducer,
		initialState,
  compose(
    applyMiddleware(thunk)
  )
	);
	sessionService.initSessionService(store);
	return store;
}
