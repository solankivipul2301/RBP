import {
  Alerts,
  Badges,
  Breadcrumbs,
  ButtonDropdowns,
  ButtonGroups,
  Buttons,
  Cards,
  Carousels,
  Charts,
  Collapses,
  Colors,
  Dashboard,
  Dropdowns,
  Flags,
  FontAwesome,
  Forms,
  Jumbotrons,
  ListGroups,
  Modals,
  Navbars,
  Navs,
  Paginations,
  Popovers,
  ProgressBar,
  SimpleLineIcons,
  BrandButtons,
  Switches,
  Tables,
  Tabs,
  Tooltips,
  Typography,
  Widgets,
	Customers,
	Locations,
  Login,
  Register,
  Profile,
  PasswordReset,
  NewPassword,
  Pricing,
  Subscription,
  Features,
  AccountDetails,
  Contacts,
  InviteDetails,
  Schedule,
  AcceptInvitation
} from './views';
import setting from './views/Setting';
import businessSetting from './views/Setting/businessSetting';
//import Register from './views/Pages/Register/Register';
import Full from './containers/Full';
import Home from './containers/Home/index';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: Full },
];

export default routes;
